function visiblePwd() {

    let clickpwd = document.getElementById("pwd");
    let visible = document.getElementById("icon");
    let incognito = document.getElementById("iconClose");

    if (clickpwd.type ==="password"){
        clickpwd.type = "text"
        visible.style.display = "block";
        incognito.style.display = "none";   
    }
     else {
            clickpwd.type = "password"
            visible.style.display = "none";
            incognito.style.display = "block";     

    }
    
} 